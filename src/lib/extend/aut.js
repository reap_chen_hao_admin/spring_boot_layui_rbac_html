layui.define(function(exports) {
    var $ = layui.$,
        admin = layui.admin;

    var aut = {
        data: function(data){
            if (data.length > 0 && Object.prototype.toString.call(data)== '[object Array]' && data[0].hasOwnProperty("alias") && data[0].hasOwnProperty("id")) {
                data.forEach((item)=>{
                    var b = false;
                    admin.req({
                        url: '/mrc/rbac/aut?alias=' + item.alias,
                        type: 'get',
                        success: function (res) {
                            b = res.data;
                            if (!b) {
                                $('#' + item.id + '').attr("disabled", "disabled");
                                $('#' + item.id + '').addClass("layui-btn-disabled");
                            }
                        }
                    });
                })
            } else{
                throw new Error("非法数据")
            }
        }
    }
    
    exports('aut', aut);
})