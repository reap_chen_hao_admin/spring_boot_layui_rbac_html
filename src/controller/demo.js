layui.define(['form', 'upload', 'table', 'aut'], function (exports) {
    var $ = layui.$,
        layer = layui.layer,
        setter = layui.setter,
        view = layui.view,
        admin = layui.admin,
        form = layui.form,
        table = layui.table,
        aut = layui.aut;

    var ok = setter.response.statusCode.ok;
    var fail = setter.response.statusCode.fail;
    var error = setter.response.statusCode.error;
    var forbidden = setter.response.statusCode.forbidden;


    //第一个实例
    var myTable = table.render({
        elem: '#LAY-demo-testDemo-table',
        url: '/mrc/test',
        page: true,
        loading: true,
        toolbar: true,
        headers: {
            access_token: layui.data('layuiAdmin').access_token
        },
        cols: [
            [{
                    checkbox: true
                },
                {
                    field: 'id',
                    title: 'ID'
                }, {
                    field: 'name',
                    title: '用户名'
                }
            ]
        ]
    });

    aut.data([{
            alias: "demo::add",
            id: "LAY-demo-testDemo-table-add"
        },
        {
            alias: "demo::update",
            id: "LAY-demo-testDemo-table-edit"
        },
        {
            alias: "demo::delete",
            id: "LAY-demo-testDemo-table-batchdel"
        },
        {
            alias: "demo::get",
            id: "LAY-demo-testDemo-table-detail"
        },
        {
            alias: "demo::get",
            id: "LAY-demo-testDemo-table-search-btn"
        }
    ])

    function table_reload() {
        myTable.reload({
            url: '/mrc/test'
        })
    }

    form.on('submit(LAY-demo-testDemo-table-search-btn)', function (data) {
        var field = data.field;

        table.reload('LAY-demo-testDemo-table', {
            url: '/mrc/test',
            where: field
        });

        return false;
      });

    var active = {
        demo_batchdel: function () {
            var checkStatus = table.checkStatus('LAY-demo-testDemo-table'),
                checkData = checkStatus.data;
            if (checkData.length === 0) {
                return layer.msg('请选择数据', function(){ });
            }
            var ids = [];
            checkData.forEach(function (a) {
                ids.push(a.id);
            })
            layer.confirm('真的删除吗？', {
                btn: ['确定','取消'] //按钮
              }, function(){
                var load = layer.load();
                admin.req({
                    type: "delete",
                    url: "/mrc/test",
                    data: JSON.stringify(ids),
                    dataType: 'json',
                    contentType: "application/json",
                    success: function (res) {
                        layer.close(load);
                        if (res.code == ok) {
                            layer.msg("删除成功", {
                                icon: 1
                            })
                        } else if (res.code == fail) {
                            layer.msg("删除失败！请重试！", {
                                icon: 1
                            })
                        } else if (res.code == forbidden) {
                            layer.msg(res.msg, {
                                icon: 2
                        })
                        } else {
                            layer.msg("系统错误，请联系开发商！", {
                                icon: 2
                            })
                        }
                        table_reload();
                    },
                    error: function () {
                        layer.msg("系统错误！请联系开发商！", {
                            icon: 0
                        })
                        layer.close(load);
                    }
                });
              }, function(){
                layer.msg("取消操作",{
                    icon: 0
                })
              });
            
        },
        demo_add: function () {
            admin.popup({
                title: '新增',
                area: ['30%', '40%'],
                id: 'LAY-popup-demo-testDemo-add',
                success: function (layero, index) {
                    view(this.id).render('demo/testDemo/demoForm').done(function () {
                        form.render(null, 'LAY-demo-testDemo-form');

                        //监听提交
                        form.on('submit(LAY-demo-testDemo-form-submit)', function (data) {
                            var load = layer.load();

                            var field = data.field;

                            admin.req({
                                type: "post",
                                url: "/mrc/test",
                                data: JSON.stringify(field),
                                dataType: 'json',
                                contentType: "application/json",
                                success: function (res) {
                                    layer.close(load);
                                    if (res.code == ok) {
                                        layer.msg("新值成功", {
                                            icon: 1
                                        })
                                    } else if (res.code == fail) {
                                        layer.msg("新增失败！请重试！", {
                                            icon: 1
                                        })
                                    }  else if (res.code == forbidden) {
                                        layer.msg(res.msg, {
                                            icon: 2
                                    })
                                    } else {
                                        layer.msg("系统错误，请联系开发商！", {
                                            icon: 2
                                        })
                                    }
                                },
                                error: function () {
                                    layer.msg("系统错误！请联系开发商！", {
                                        icon: 0
                                    })
                                    layer.close(load);
                                }
                            });

                            layer.close(index); //执行关闭 
                        });
                    });
                },
                end: function () {
                    table_reload();
                }
            });
        },
        demo_detail: function () {
            var checkStatus = table.checkStatus('LAY-demo-testDemo-table'),
                checkData = checkStatus.data;
            if (checkData.length == 0) {
                return layer.msg('请选择数据', function(){ });
            }
            if (checkData.length > 1) {
                return layer.msg('只可以选择一条数据', function(){ });
            }

                admin.popup({
                    title: '详情',
                    area: ['30%', '40%'],
                    id: 'LAY-popup-demo-testDemo-add',
                    zIndex: layer.zIndex,
                    success: function (layero, index) {
                        view(this.id).render('demo/testDemo/detail', checkData[0]);
                    }
                })

        },
        demo_edit: function () {
            var checkStatus = table.checkStatus('LAY-demo-testDemo-table'),
                checkData = checkStatus.data;
            if (checkData.length == 0) {
                return layer.msg('请选择数据', function(){ });
            }
            if (checkData.length > 1) {
                return layer.msg('只可以选择一条数据', function(){ });
            }

            admin.popup({
                title: '编辑',
                area: ['30%', '40%'],
                id: 'LAY-popup-demo-testDemo-add',
                success: function (layero, index) {
                    view(this.id).render('demo/testDemo/demoForm', checkData[0]).done(function () {
                        form.render(null, 'LAY-demo-testDemo-form');

                        //监听提交
                        form.on('submit(LAY-demo-testDemo-form-submit)', function (data) {
                            var load = layer.load();

                            var field = data.field;

                            admin.req({
                                type: "put",
                                url: "/mrc/test",
                                data: JSON.stringify(field),
                                dataType: 'json',
                                contentType: "application/json",
                                success: function (res) {
                                    layer.close(load);
                                    if (res.code == ok) {
                                        layer.msg("修改成功", {
                                            icon: 1
                                        })
                                    } else if (res.code == fail) {
                                        layer.msg("修改失败！请重试！", {
                                            icon: 1
                                        })
                                    } else if (res.code == forbidden) {
                                        layer.msg(res.msg, {
                                            icon: 2
                                    })
                                    } else {
                                        layer.msg("系统错误，请联系开发商！", {
                                            icon: 2
                                        })
                                    }
                                },
                                error: function () {
                                    layer.msg("系统错误！请联系开发商！", {
                                        icon: 0
                                    })
                                    layer.close(load);
                                }
                            });

                            layer.close(index); //执行关闭 
                        });
                    });
                },
                end: function () {
                    table_reload();
                }
            });
        }
    }



    $('.layui-btn.layuiadmin-btn-admin').on('click', function () {
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });



    exports('demo', {});
})