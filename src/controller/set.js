/**

 @Name：layuiAdmin 设置
 @Author：贤心
 @Site：http://www.layui.com/admin/
 @License: LPPL
    
 */

layui.define(['form', 'aut'], function (exports) {
  var $ = layui.$,
    layer = layui.layer,
    setter = layui.setter,
    view = layui.view,
    admin = layui.admin,
    form = layui.form,
    aut = layui.aut;
  ;

  var ok = setter.response.statusCode.ok;
  var fail = setter.response.statusCode.fail;
  var error = setter.response.statusCode.error;
  var forbidden = setter.response.statusCode.forbidden;

  var $body = $('body');

  form.render();

  //自定义验证
  form.verify({
    nickname: function (value, item) { //value：表单的值、item：表单的DOM对象
        if (!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)) {
          return '用户名不能有特殊字符';
        }
        if (/(^\_)|(\__)|(\_+$)/.test(value)) {
          return '用户名首尾不能出现下划线\'_\'';
        }
        if (/^\d+\d+\d$/.test(value)) {
          return '用户名不能全为数字';
        }
      }

      //我们既支持上述函数式的方式，也支持下述数组的形式
      //数组的两个值分别代表：[正则匹配、匹配不符时的提示文字]
      ,
    pass: [
        /^[\S]{6,12}$/, '密码必须6到12位，且不能出现空格'
      ]

      //确认密码
      ,
    repass: function (value) {
      if (value !== $('#LAY_password').val()) {
        return '两次密码输入不一致';
      }
    }
  });

  aut.data([
    {id:"setmypass",alias:"user::resetPwd"}
  ])


  //设置密码
  form.on('submit(setmypass)', function (obj) {
    var field = obj.field;
    delete field.oldPassword;
    delete field.repassword;

    admin.req({
      type: "put",
      url: "/mrc/user/resetPwd",
      data: JSON.stringify(field),
      dataType: "json",
      contentType: "application/json",
      success: function (res) {
        if (res.code == ok) {
          layer.msg("修改成功", {
            icon: 1
          },function(){
            admin.exit();
          })
        } else if (res.code == forbidden) {
          layer.msg(res.msg, {
            icon: 2
          })
        } else {
          layer.msg("修改失败，请重试！", {
            icon: 0
          })
        }

        form.val("LAY-set-user-password", {
          "oldPassword": "",
          "password": "",
          "repassword": "",
        });
      }
    });
    return false;
  });

  //对外暴露的接口
  exports('set', {});
});