layui.define(['form', 'upload', 'table', 'aut', 'xmSelect'], function (exports) {
    var $ = layui.$,
        layer = layui.layer,
        setter = layui.setter,
        view = layui.view,
        admin = layui.admin,
        form = layui.form,
        table = layui.table,
        xmSelect = layui.xmSelect,
        aut = layui.aut;

    var ok = setter.response.statusCode.ok;
    var fail = setter.response.statusCode.fail;
    var error = setter.response.statusCode.error;
    var forbidden = setter.response.statusCode.forbidden;


    var myTable = table.render({
        elem: '#LAY-set-dict-table',
        url: '/mrc/public/sysDictTypeSelect',
        // page: true,
        loading: true,
        toolbar: true,
        headers: {
            access_token: layui.data('layuiAdmin').access_token
        },
        cols: [
            [{
                    checkbox: true
                },
                {
                    field: 'dictTypeId',
                    title: 'ID'
                }, {
                    field: 'typeName',
                    title: '字典类型'
                }
            ]
        ]
    });

    aut.data([{
            alias: "dict::add",
            id: "LAY-set-dict-table-add"
        },
        {
            alias: "dict::update",
            id: "LAY-set-dict-table-edit"
        }, {
            alias: "dict::delete",
            id: "LAY-set-dict-table-delete"
        }, {
            alias: "dict::get",
            id: "LAY-set-dict-table-detail"
        }
    ])



    function table_reload() {
        myTable.reload({
            url: '/mrc/public/sysDictTypeSelect'
        })
    }

    var active = {
        dict_add: function () {
            var checkStatus = table.checkStatus('LAY-set-dict-table'),
                checkData = checkStatus.data;
            if (checkData.length == 0) {
                return layer.msg('请选择数据', function () {});
            }
            if (checkData.length > 1) {
                return layer.msg('只可以选择一条数据', function () {});
            }
            checkData[0].t = "add";
            admin.popup({
                title: '新增',
                area: ['25%', '30%'],
                id: 'LAY-popup-set-dict-add',
                success: function (layero, index) {
                    view(this.id).render('set/dict/dictForm', checkData[0]).done(function () {
                        form.render(null, 'LAY-set-dict-form');



                        //监听提交
                        form.on('submit(LAY-set-dict-form-submit)', function (data) {
                            var load = layer.load();

                            var field = data.field;

                            admin.req({
                                type: "post",
                                url: "/mrc/sys-dict-value",
                                data: JSON.stringify(field),
                                dataType: 'json',
                                contentType: "application/json",
                                success: function (res) {
                                    layer.close(load);
                                    if (res.code == ok) {
                                        layer.msg("新增成功", {
                                            icon: 1
                                        })
                                    } else if (res.code == fail) {
                                        layer.msg("新增失败！请重试！", {
                                            icon: 2
                                        })
                                    } else if (res.code == forbidden) {
                                        layer.msg(res.msg, {
                                            icon: 2
                                        })
                                    } else {
                                        layer.msg("系统错误，请联系开发商！", {
                                            icon: 2
                                        })
                                    }
                                },
                                error: function () {
                                    layer.msg("系统错误！请联系开发商！", {
                                        icon: 0
                                    })
                                    layer.close(load);
                                }
                            });

                            layer.close(index); //执行关闭 
                        });
                    });
                },
                end: function () {
                    table_reload();
                }
            });
        },
        dict_edit: function () {
            var checkStatus = table.checkStatus('LAY-set-dict-table'),
                checkData = checkStatus.data;
            if (checkData.length == 0) {
                return layer.msg('请选择数据', function () {});
            }
            if (checkData.length > 1) {
                return layer.msg('只可以选择一条数据', function () {});
            }

            admin.popup({
                title: '编辑',
                area: ['25%', '50%'],
                id: 'LAY-popup-set-dict-add',
                success: function (layero, index) {
                    view(this.id).render('set/dict/dictForm', checkData[0]).done(function () {
                        form.render(null, 'LAY-set-dict-form');

                        var xm = select('#LAY-set-dict-form-valueSelect', checkData[0]);

                        //监听提交
                        form.on('submit(LAY-set-dict-form-submit)', function (data) {
                            var load = layer.load();

                            var field = data.field;
                            delete field.select;
                            var selectVal = xm.getValue("valueStr");
                            field.dictId = selectVal;

                            admin.req({
                                type: "put",
                                url: "/mrc/sys-dict-value",
                                data: JSON.stringify(field),
                                dataType: 'json',
                                contentType: "application/json",
                                success: function (res) {
                                    layer.close(load);
                                    if (res.code == ok) {
                                        layer.msg("修改成功", {
                                            icon: 1
                                        })
                                    } else if (res.code == fail) {
                                        layer.msg("修改失败！请重试！", {
                                            icon: 2
                                        })
                                    } else if (res.code == forbidden) {
                                        layer.msg(res.msg, {
                                            icon: 2
                                        })
                                    } else {
                                        layer.msg("系统错误，请联系开发商！", {
                                            icon: 2
                                        })
                                    }
                                },
                                error: function () {
                                    layer.msg("系统错误！请联系开发商！", {
                                        icon: 0
                                    })
                                    layer.close(load);
                                }
                            });

                            layer.close(index); //执行关闭 
                        });
                    });
                },
                end: function () {
                    table_reload();
                }
            });
        },
        dict_del: function () {
            var checkStatus = table.checkStatus('LAY-set-dict-table'),
                checkData = checkStatus.data;
            if (checkData.length == 0) {
                return layer.msg('请选择数据', function () {});
            }
            if (checkData.length > 1) {
                return layer.msg('只可以选择一条数据', function () {});
            }
            checkData[0].t = "del";
            admin.popup({
                title: '删除',
                area: ['25%', '50%'],
                id: 'LAY-popup-set-dict-add',
                success: function (layero, index) {
                    view(this.id).render('set/dict/dictForm', checkData[0]).done(function () {
                        form.render(null, 'LAY-set-dict-form');

                        var xm = select('#LAY-set-dict-form-valueSelect2', checkData[0]);

                        form.on('submit(LAY-set-dict-form-submit)', function (data) {
                            var load = layer.load();
                            admin.req({
                                type: "delete",
                                url: "/mrc/sys-dict-value",
                                data: {
                                   dictId: xm.getValue("valueStr")
                                },
                                success: function (res) {
                                    layer.close(load);
                                    if (res.code == ok) {
                                        layer.msg("删除成功", {
                                            icon: 1
                                        })
                                    } else if (res.code == fail) {
                                        layer.msg("删除失败！请重试！", {
                                            icon: 2
                                        })
                                    } else if (res.code == forbidden) {
                                        layer.msg(res.msg, {
                                            icon: 2
                                        })
                                    } else {
                                        layer.msg("系统错误，请联系开发商！", {
                                            icon: 2
                                        })
                                    }
                                },
                                error: function () {
                                    layer.msg("系统错误！请联系开发商！", {
                                        icon: 0
                                    })
                                    layer.close(load);
                                }
                            });

                            layer.close(index);
                        })

                    });
                },
                end: function () {
                    table_reload();
                }
            });
        },
        dict_detail: function () {
            var checkStatus = table.checkStatus('LAY-set-dict-table'),
                checkData = checkStatus.data;
            if (checkData.length == 0) {
                return layer.msg('请选择数据', function () {});
            }
            if (checkData.length > 1) {
                return layer.msg('只可以选择一条数据', function () {});
            }
            checkData[0].t = "add";
            admin.popup({
                title: '详情',
                area: ['25%', '45%'],
                id: 'LAY-popup-set-dict-add',
                success: function (layero, index) {
                    view(this.id).render('set/dict/detail', checkData[0]).done(function () {
                        form.render(null, 'LAY-set-dict-form-detail');
                        var xm = select('#LAY-set-dict-form-valueSelect2', checkData[0]);
                    })
                }
            })
        }
    }

    var select = function (id, data) {
        var xm = xmSelect.render({
            el: id,
            filterable: true,
            paging: true,
            radio: true,
            clickClose: true,
            pageEmptyShow: false,
            layVerify: 'required',
            pageSize: 3,
            prop: {
                name: 'value',
                value: 'dictId',
            },
            data: []
        })

        admin.req({
            type: "get",
            url: "/mrc/public/sysDictValueSelect",
            data: {
                type: data.typeName
            },
            success: function (res) {
                if (res.code == 200) {
                    xm.update({
                        data: res.data,
                        autoRow: true,
                    })
                }
            },
            error: function () {
                xm.update({
                    data: [],
                    autoRow: true,
                    disable: true
                })
            }
        })

        return xm;
    }



    $('.layui-btn.layuiadmin-btn-admin').on('click', function () {
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });

    exports('dict', {});
})